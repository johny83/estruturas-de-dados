#ifndef TREE_H_INCLUDED
#define TREE_H_INCLUDED

using namespace std;

// CLASSE PARA AS FUNCIONALIDADES DA ARVORE
template <class T>
class BST {
    protected:
        BinaryTree<T> *root;
    public:
        BST(); // Construtor
        BST(T data); // Construtor

        BinaryTree<T>* getRoot();
        void addNode(T data);
        bool isEmpty();
        void preOrder(BinaryTree<T> *node, bool isLimit);
        void postOrder(BinaryTree<T> *node, bool isLimit);
        void printList(BinaryTree<T> *node);
        void printLeaf(BinaryTree<T> *node, bool isLimit);
        void printTreeInBarMode(BinaryTree<T> *node, string valor);
};

template <typename T>
BST<T> :: BST()
{
    this->root = NULL;
}

template <typename T>
BST<T> :: BST(T data)
{
    this->root = new BinaryTree<T>(data);
}

template <typename T>
void BST<T> :: addNode(T data)
{
    BinaryTree<T> *temp = getRoot();
    BinaryTree<T> *parent = NULL;

    while(temp != NULL){
        parent = temp;
        if(data > temp->GetData())
            temp = temp->right.get();
        else
            temp = temp->left.get();
    }

    if(isEmpty())
        this->root = new BinaryTree<T>(data);
    else {
        if(data > parent->GetData())
            parent->right.reset(new BinaryTree<T>(data));
        else
            parent->left.reset(new BinaryTree<T>(data));
    }
}

template <typename T>
bool BST<T> :: isEmpty()
{
    return getRoot() == NULL;
}

template <typename T>
BinaryTree<T>* BST<T> :: getRoot()
{
    return this->root;
}

template <typename T>
void BST<T> :: printList(BinaryTree<T> *root){
	if(!isEmpty()){
		cout << root->GetData() << ' ';
        preOrder(root->left.get(), true);
        postOrder(root->right.get(), true);
	}
}

template <typename T>
void BST<T> :: preOrder(BinaryTree<T> *node, bool isLimit){
	if(node){
        printLeaf(node,isLimit);
        preOrder(node->left.get(), isLimit);
        preOrder(node->right.get(), isLimit && !node->left.get());
	}
}

template <typename T>
void BST<T> :: postOrder(BinaryTree<T> *node, bool isLimit){
	if(node){
        postOrder(node->left.get(), isLimit && !node->right.get());
        postOrder(node->right.get(), isLimit);
        printLeaf(node,isLimit);
	}
}

template <typename T>
void BST<T> :: printLeaf(BinaryTree<T> *node, bool isLimit){
    if (isLimit || (!node->left.get() && !node->right.get())) {
        cout << node->GetData() << ' ';
    }
}

template <typename T>
void BST<T> :: printTreeInBarMode(BinaryTree<T> *node, string valor){
	if(node!=NULL){
		cout << valor << node->GetData() << endl;
	}
	else{
		return;
	}
	if(node->left!=NULL){
		printTreeInBarMode(node->left.get(), valor+"--");
	}
	if(node->right!=NULL){
		printTreeInBarMode(node->right.get(), valor+"--");
	}
}

#endif // TREE_H_INCLUDED
