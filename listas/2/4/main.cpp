#include <iostream>
#include <memory>
#include <ctime>
#include <stack>
#include <cstdlib>
#include <stdio.h>

#include "node.hpp"
#include "tree.hpp"

using namespace std;

int main(int argc, char** argv) {

    int A[16] = {42, 35, 68, 1, 25, 59, 70, 6, 28, 46, 63, 79, 82, 92, 62, 80};

    // cria n�s
    BST<int> *tree = new BST<int>();

    for(int i = 0; i < 16; i++){
        tree->addNode(A[i]);
    }

    tree->printTreeInBarMode(tree->getRoot(),"");

    tree->printList(tree->getRoot());

    return 0;
}
