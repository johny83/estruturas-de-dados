#ifndef NODE_H_INCLUDED
#define NODE_H_INCLUDED

using namespace std;

// CLASSE PARA O N� DA �RVORE
template <typename T>
class BinaryTree {
    public:
        T data;
        shared_ptr <BinaryTree<T>> left, right;

        BinaryTree(); // Construtor
        BinaryTree(T data); // Construtor
        T GetData() const;
};

template <typename T>
BinaryTree<T> :: BinaryTree() : left(nullptr), right(nullptr)
{
    this->data = 0;
    this->left = this->right = NULL;
}

template <typename T>
BinaryTree<T> :: BinaryTree(T data) : left(nullptr), right(nullptr)
{
    this->data = data;
    this->left = this->right = NULL;
}

template <typename T>
T BinaryTree<T> :: GetData() const
{
    return data;
}

#endif // NODE_H_INCLUDED
