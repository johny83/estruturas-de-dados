#ifndef TREE_H_INCLUDED
#define TREE_H_INCLUDED

using namespace std;

// CLASSE PARA AS FUNCIONALIDADES DA �RVORE
template <class T>
class BST{
    protected:
        BinaryTree<T> *root;
    public:
        BST(); // Construtor
        BST(T data); // Construtor

        BinaryTree<T>* getRoot();
        BinaryTree<T>* findNonKBalancedeNode(BinaryTree<T> *root, int k);
        BinaryTree<T>* findNonKBalancedeNode(int k);
        void addNode(T data);
        void printTreeInBarMode(BinaryTree<T> *node, string valor);
        bool isEmpty();
        bool isBalanced(BinaryTree<T> *node, int k);
        int height(BinaryTree<T> *node);
};

template <typename T>
BST<T> :: BST()
{
    this->root = NULL;
}

template <typename T>
BST<T> :: BST(T data)
{
    this->root = new BinaryTree<T>(data);
}

template <typename T>
void BST<T> :: addNode(T data)
{
    BinaryTree<T> *temp = getRoot();
    BinaryTree<T> *parent = NULL;

    while(temp != NULL){
        parent = temp;
        if(data > temp->GetData())
            temp = temp->right.get();
        else
            temp = temp->left.get();
    }

    if(isEmpty())
        this->root = new BinaryTree<T>(data);
    else {
        if(data > parent->GetData())
            parent->right.reset(new BinaryTree<T>(data));
        else
            parent->left.reset(new BinaryTree<T>(data));
    }
}

template <typename T>
bool BST<T> :: isEmpty()
{
    return getRoot() == NULL;
}

template <typename T>
BinaryTree<T>* BST<T> :: getRoot()
{
    return this->root;
}

template <typename T>
bool BST<T> :: isBalanced(BinaryTree<T> *root, int k)
{
    if (root == NULL) return 1;

    if ( abs(height(root->left.get()) - height(root->right.get())) <= k ) return 1;

    return 0;
}

template <typename T>
int BST<T> :: height(BinaryTree<T> *node){
    if(node == NULL){
        return 0;
    } else {
        return 1 + max(height(node->left.get()), height(node->right.get()));
    }
}

template <typename T>
BinaryTree<T>* BST<T> :: findNonKBalancedeNode(int k){
    return findNonKBalancedeNode(getRoot(), k);
}

template <typename T>
BinaryTree<T>* BST<T> :: findNonKBalancedeNode(BinaryTree<T> *root, int k){

    if(root == NULL) return NULL;

    if (!isBalanced(root, k)){
        if (isBalanced(root->left.get(),k) && isBalanced(root->right.get(),k)) return root;
    }

    BinaryTree<T> *left = findNonKBalancedeNode(root->left.get(), k);
    if(left) return left;

    BinaryTree<T> *right = findNonKBalancedeNode(root->right.get(), k);
    if(right) return right;

    return NULL;

}

template <typename T>
void BST<T> :: printTreeInBarMode(BinaryTree<T> *node, string valor){
	if(node!=NULL){
		cout << valor << node->GetData() << endl;
	}
	else{
		return;
	}
	if(node->left!=NULL){
		printTreeInBarMode(node->left.get(), valor+"--");
	}
	if(node->right!=NULL){
		printTreeInBarMode(node->right.get(), valor+"--");
	}
}

#endif // TREE_H_INCLUDED
