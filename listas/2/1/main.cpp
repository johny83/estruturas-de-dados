#include <iostream>
#include <memory>
#include <ctime>
#include <cstdlib>
#include <stdio.h>

#include "node.hpp"
#include "tree.hpp"

using namespace std;

int main(int argc, char** argv) {

    int A[16] = {42, 35, 68, 1, 25, 59, 70, 6, 28, 46, 63, 79, 82, 92, 62, 80};

    // cria n�s
    BST<int> *tree = new BST<int>();

    for(int i = 0; i < 16; i++){
        tree->addNode(A[i]);
    }

    // imprime a arvore em modo de barra
    tree->printTreeInBarMode(tree->getRoot(),"");

    cout << " " << endl;

    BinaryTree<int> *node = new BinaryTree<int>();
    node = tree->findNonKBalancedeNode(2);

    if(node != NULL)
        cout << "O valor para o do no para o k (2) nao balanceado e " << node->GetData() << endl;
    else
        cout << "O ponteiro e NULO, pois todos nos estao k-balanceados" << endl;

    return 0;
}
