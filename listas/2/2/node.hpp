#ifndef NODE_H_INCLUDED
#define NODE_H_INCLUDED

using namespace std;

// CLASSE PARA O N� DA �RVORE
template <typename T>
class BinaryTree {
    public:
        T data;
        shared_ptr <BinaryTree<T>> left, right, parent;
        bool locked = false;
        int numLocks;

        BinaryTree(); // Construtor
        BinaryTree(T data); // Construtor
        T GetData() const;

        bool isLock();
        bool lock();
        bool parentLocked();
        void countLocks();
        void unLock();
};

template <typename T>
BinaryTree<T> :: BinaryTree() : left(nullptr), right(nullptr), parent(nullptr)
{
    this->data = 0;
    this->left = this->right = this->parent = NULL;
}

template <typename T>
BinaryTree<T> :: BinaryTree(T data) : left(nullptr), right(nullptr), parent(nullptr)
{
    this->data = data;
    this->left = this->right = this->parent = NULL;
}

template <typename T>
T BinaryTree<T> :: GetData() const
{
    return data;
}

template <typename T>
bool BinaryTree<T> :: isLock()
{
    return locked;
}

template <typename T>
bool BinaryTree<T> :: lock()
{

    // Verifica a quantidade de descendentes ou se o n� atual j� foi bloqueado
    if (this->numLocks > 0 || this->locked)
        return false;

    // Checa se existe algum descendente bloqueado
    else if (parentLocked())
        return false;

    // O n� atual � bloqueado
    this->locked = true;

    // Realiza a contagem de n�s descendentes
    countLocks();

    return true;
}

template <typename T>
void BinaryTree<T> :: unLock()
{
    if (this->locked) {
        this->locked = false;
        shared_ptr<BinaryTree<T>> node = this->parent;
        while(node) {
            node->numLocks--;
            node = node->parent;
        }
    }
}

template <typename T>
bool BinaryTree<T> :: parentLocked()
{
    for (auto it = parent; it != NULL; it = it->parent){
        if (it->locked){
            return true;
        }
    }
    return false;
}

template <typename T>
void BinaryTree<T> :: countLocks()
{
    for (auto it = parent; it != NULL; it = it->parent){
        it->numLocks++;
    }
}

#endif // NODE_H_INCLUDED
