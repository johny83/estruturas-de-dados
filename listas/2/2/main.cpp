#include <cassert>
#include <iostream>
#include <memory>
#include <ctime>
#include <cstdlib>
#include <stdio.h>

#include "node.hpp"
#include "tree.hpp"

using namespace std;

int main(int argc, char** argv) {

    int A[16] = {42, 35, 68, 1, 25, 59, 70, 6, 28, 46, 63, 79, 82, 92, 62, 80};

    // cria n�s
    BST<int> *tree = new BST<int>();

    for(int i = 0; i < 16; i++){
        tree->addNode(A[i]);
    }

    BinaryTree<int> *root = new BinaryTree<int>();

    root = tree->getRoot();

    cout << "A ARVORE ORIGINAL COM OS STATUS NAO BLOQUEADOS" << endl;
    cout << "" << endl;
    tree->printTreeInBarModeWithStatus(root,"");

    cout << "" << endl;
    cout << "==================================" << endl;
    cout << "BLOQUEIA O NO 70 DA ARVORE" << endl;
    cout << "" << endl;

    tree->setLock(root,70);
    tree->printTreeInBarModeWithStatus(root,"");

    cout << "" << endl;
    cout << "==================================" << endl;
    cout << "BLOQUEIA O NO 68 DA ARVORE" << endl;
    cout << "(mas nao e possivel, pois e descendente de 70" << endl;
    cout << "que ja esta bloqueado)" << endl;
    cout << "" << endl;

    tree->setLock(root,68);
    tree->printTreeInBarModeWithStatus(root,"");

    cout << "" << endl;
    cout << "==================================" << endl;
    cout << "BLOQUEIA O NO 6 DA ARVORE" << endl;
    cout << "" << endl;

    tree->setLock(root,6);
    tree->printTreeInBarModeWithStatus(root,"");

    cout << "" << endl;
    cout << "==================================" << endl;
    cout << "DESBLOQUEIA O NO 70 DA ARVORE" << endl;
    cout << "" << endl;

    tree->setUnLock(root,70);
    tree->printTreeInBarModeWithStatus(root,"");

    cout << "" << endl;
    cout << "==================================" << endl;
    cout << "BLOQUEIA O NO 79 DA ARVORE" << endl;
    cout << "" << endl;

    tree->setLock(root,79);
    tree->printTreeInBarModeWithStatus(root,"");

    return 0;
}
