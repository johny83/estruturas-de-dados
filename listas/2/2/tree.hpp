#ifndef TREE_H_INCLUDED
#define TREE_H_INCLUDED

using namespace std;

// CLASSE PARA AS FUNCIONALIDADES DA �RVORE
template <class T>
class BST{
    protected:
        BinaryTree<T> *root;
    public:
        BST(); // Construtor
        BST(T data); // Construtor

        BinaryTree<T>* getRoot();
        void addNode(T data);
        void printTreeInBarModeWithStatus(BinaryTree<T> *node, string valor);
        bool isEmpty();

        bool setLock(BinaryTree<T> *node, int value);
        bool setUnLock(BinaryTree<T> *node, int value);
};

template <typename T>
BST<T> :: BST()
{
    this->root = NULL;
}

template <typename T>
BST<T> :: BST(T data)
{
    this->root = new BinaryTree<T>(data);
}

template <typename T>
void BST<T> :: addNode(T data)
{
    BinaryTree<T> *temp = getRoot();
    BinaryTree<T> *parent = NULL;

    while(temp != NULL){
        parent = temp;
        if(data > temp->GetData())
            temp = temp->right.get();
        else
            temp = temp->left.get();
    }

    if(isEmpty()) {
        this->root = new BinaryTree<T>(data);
        this->root->parent = NULL;
    } else {
        if(data > parent->GetData()) {
            parent->right.reset(new BinaryTree<T>(data));
            parent->right->parent.reset(parent);
        } else {
            parent->left.reset(new BinaryTree<T>(data));
            parent->left->parent.reset(parent);
        }
    }
}

template <typename T>
bool BST<T> :: isEmpty()
{
    return getRoot() == NULL;
}

template <typename T>
BinaryTree<T>* BST<T> :: getRoot()
{
    return this->root;
}

template <typename T>
void BST<T> :: printTreeInBarModeWithStatus(BinaryTree<T> *node, string valor){
	if(node!=NULL){
		cout << valor << node->GetData() << " (" << node->isLock() << ")" <<endl;
	}
	else{
		return;
	}
	if(node->left!=NULL){
		printTreeInBarModeWithStatus(node->left.get(), valor+"--");
	}
	if(node->right!=NULL){
		printTreeInBarModeWithStatus(node->right.get(), valor+"--");
	}
}

template <typename T>
bool BST<T> :: setLock(BinaryTree<T> *node, int value)
{
    if(node == NULL) return false;

    if(node->data == value){
        return node->lock();
    }

    if(value > node->data){
        return setLock(node->right.get(), value);
    } else {
        return setLock(node->left.get(), value);
    }
}

template <typename T>
bool BST<T> :: setUnLock(BinaryTree<T> *node, int value)
{
    if(node == NULL) return false;

    if(node->data == value){
        node->unLock();
    }

    if(value > node->data){
        return setUnLock(node->right.get(), value);
    } else {
        return setUnLock(node->left.get(), value);
    }
}

#endif // TREE_H_INCLUDED
