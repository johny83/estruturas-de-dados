#include <iostream>
#include <memory>
#include <ctime>
#include <cstdlib>
#include <stdio.h>
#include <vector>
#define MAX 16

#include "node.hpp"
#include "tree.hpp"

using namespace std;

int main(int argc, char** argv) {

    int A[MAX] = {42, 35, 68, 1, 25, 59, 70, 6, 28, 46, 63, 79, 82, 92, 62, 80};

    // cria n�s
    BST<int> *tree = new BST<int>();

    for(int i = 0; i < MAX; i++){
        tree->addNode(A[i]);
    }

    cout << "ARVORE ORIGINAL" << endl;
    cout << "" << endl;
    tree->printTreeInBarMode(tree->getRoot(),"");
    cout << "" << endl;
    cout << "==================================" << endl;
    cout << "" << endl;
    cout << "IMPRIMINDO EM ORDEM E POS-ORDEM" << endl;
    cout << "" << endl;
    cout << "Em-ordem: ";
    tree->printInOrder(tree->getRoot());
    cout << "" << endl;
    cout << "Pos-ordem: ";
    tree->printPostOrder(tree->getRoot());
    cout << "" << endl;
    cout << "" << endl;
    cout << "==================================" << endl;
    cout << "" << endl;
    cout << "CONSTRUINDO NOVA ARVORE ATRAVES DO" << endl;
    cout << "PERCORRIMENTO EM ORDEM E POS-ORDEM" << endl;
    cout << "" << endl;
    BinaryTree<int> *root = tree->buildTree();
    tree->printTreeInBarMode(root,"");

    return 0;
}
