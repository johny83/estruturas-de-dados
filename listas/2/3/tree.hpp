#ifndef TREE_H_INCLUDED
#define TREE_H_INCLUDED

using namespace std;

// CLASSE PARA AS FUNCIONALIDADES DA �RVORE
template <class T>
class BST{
    protected:
        BinaryTree<T> *root;
    public:
        vector<int> inOrder;
        vector<int> postOrder;

        BST(); // Construtor
        BST(T data); // Construtor

        BinaryTree<T>* getRoot();
        void addNode(T data);
        void printTreeInBarMode(BinaryTree<T> *node, string valor);
        void printInOrder(BinaryTree<T> *node);
        void printPostOrder(BinaryTree<T> *node);
        bool isEmpty();
        BinaryTree<T>* buildTree();
        BinaryTree<T>* build(int instart, int inend, int poststart, int postend);
};

template <typename T>
BST<T> :: BST()
{
    this->root = NULL;
}

template <typename T>
BST<T> :: BST(T data)
{
    this->root = new BinaryTree<T>(data);
}

template <typename T>
void BST<T> :: addNode(T data)
{
    BinaryTree<T> *temp = getRoot();
    BinaryTree<T> *parent = NULL;

    while(temp != NULL){
        parent = temp;
        if(data > temp->GetData())
            temp = temp->right.get();
        else
            temp = temp->left.get();
    }

    if(isEmpty())
        this->root = new BinaryTree<T>(data);
    else {
        if(data > parent->GetData())
            parent->right.reset(new BinaryTree<T>(data));
        else
            parent->left.reset(new BinaryTree<T>(data));
    }
}

template <typename T>
bool BST<T> :: isEmpty()
{
    return getRoot() == NULL;
}

template <typename T>
BinaryTree<T>* BST<T> :: getRoot()
{
    return this->root;
}

template <typename T>
void BST<T> :: printTreeInBarMode(BinaryTree<T> *node, string valor){
	if(node!=NULL){
		cout << valor << node->GetData() << endl;
	}
	else{
		return;
	}
	if(node->left!=NULL){
		printTreeInBarMode(node->left.get(), valor+"--");
	}
	if(node->right!=NULL){
		printTreeInBarMode(node->right.get(), valor+"--");
	}
}

template <typename T>
void BST<T> :: printInOrder(BinaryTree<T> *node)
{
    if(node != NULL){
        printInOrder(node->left.get());
        inOrder.push_back(node->GetData());
        cout << node->GetData() << " ";
        printInOrder(node->right.get());
    }
}

template <typename T>
void BST<T> :: printPostOrder(BinaryTree<T> *node)
{
    if(node != NULL){
        printPostOrder(node->left.get());
        printPostOrder(node->right.get());
        postOrder.push_back(node->GetData());
        cout << node->GetData() << " ";
    }
}

template <typename T>
BinaryTree<T>* BST<T> :: buildTree()
{
    int len = inOrder.size();
    BinaryTree<T> *root = build(0, len, 0, len);
    return root;
}

template <typename T>
BinaryTree<T>* BST<T> :: build(int instart, int inend, int poststart, int postend)
{
    if ((postend - poststart) < 1 || (inend - instart) < 1) return NULL;

    int val = postOrder[postend-1];
    int split = instart;

    for (int i = instart; i != inend; i++){
        if (inOrder[i] == val){
            split = i;
        }
    }

    BinaryTree<T> *root = new BinaryTree<T>(val);
    root->left.reset(build(instart, split, poststart, (poststart + split) - instart));
    root->right.reset(build(split+1, inend, (poststart + split) - instart, postend-1));

    return root;
}

#endif // TREE_H_INCLUDED
