#ifndef TREE_H_INCLUDED
#define TREE_H_INCLUDED

using namespace std;

// CLASSE PARA AS FUNCIONALIDADES DA �RVORE
template <class T>
class BST{
    protected:
        BinaryTree<T> *root;
    public:
        BST(); // Construtor
        BST(T data); // Construtor

        BinaryTree<T>* getRoot();
        int ACMB(BinaryTree<T> *node, int A, int B);
        bool findPath(BinaryTree<T> *node, vector<int> &path, int k);
        bool isEmpty();
        void printTreeInBarMode(BinaryTree<T> *node, string valor);
        void addNode(T data);
};

template <typename T>
BST<T> :: BST()
{
    this->root = NULL;
}

template <typename T>
BST<T> :: BST(T data)
{
    this->root = new BinaryTree<T>(data);
}

template <typename T>
void BST<T> :: addNode(T data)
{
    BinaryTree<T> *temp = getRoot();
    BinaryTree<T> *parent = NULL;

    while(temp != NULL){
        parent = temp;
        if(data > temp->GetData())
            temp = temp->right.get();
        else
            temp = temp->left.get();
    }

    if(isEmpty())
        this->root = new BinaryTree<T>(data);
    else {
        if(data > parent->GetData())
            parent->right.reset(new BinaryTree<T>(data));
        else
            parent->left.reset(new BinaryTree<T>(data));
    }
}

template <typename T>
bool BST<T> :: isEmpty()
{
    return getRoot() == NULL;
}

template <typename T>
BinaryTree<T>* BST<T> :: getRoot()
{
    return this->root;
}

template <typename T>
int BST<T> :: ACMB(BinaryTree<T> *node, int A, int B)
{
    if (node == NULL) return NULL;

    vector<int> pathA, pathB;
    int i;

    if ( !findPath(node, pathA, A) || !findPath(node, pathB, B)) return -1;

    for (i = 0; i < pathA.size() && i < pathB.size() ; i++)
        if (pathA[i] != pathB[i]) break;

    return pathA[i-1];

}

template <typename T>
bool BST<T> :: findPath(BinaryTree<T> *node, vector<int> &path, int k)
{
    if (node == NULL) return false;

    path.push_back(node->GetData());

    if (node->GetData() == k) return true;

    if ( (node->left.get() && findPath(node->left.get(), path, k)) ||
         (node->right.get() && findPath(node->right.get(), path, k)) )
        return true;

    path.pop_back();
    return false;
}

template <typename T>
void BST<T> :: printTreeInBarMode(BinaryTree<T> *node, string valor){
	if(node!=NULL){
		cout << valor << node->GetData() << endl;
	}
	else{
		return;
	}
	if(node->left!=NULL){
		printTreeInBarMode(node->left.get(), valor+"--");
	}
	if(node->right!=NULL){
		printTreeInBarMode(node->right.get(), valor+"--");
	}
}

#endif // TREE_H_INCLUDED
