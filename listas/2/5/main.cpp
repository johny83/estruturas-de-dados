#include <iostream>
#include <memory>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <stdio.h>

#include "node.hpp"
#include "tree.hpp"

using namespace std;

int main(int argc, char** argv) {

    int A[16] = {42, 35, 68, 1, 25, 59, 70, 6, 28, 46, 63, 79, 82, 92, 62, 80};

    // cria n�s
    BST<int> *tree = new BST<int>();

    for(int i = 0; i < 16; i++){
        tree->addNode(A[i]);
    }

    tree->printTreeInBarMode(tree->getRoot(),"");

    cout << "" << endl;

    int res = tree->ACMB(tree->getRoot(),80,92);
    cout << "ACMB(80,92) = " << res << endl;

    res = tree->ACMB(tree->getRoot(),25,82);
    cout << "ACMB(25,82) = " << res << endl;

    res = tree->ACMB(tree->getRoot(),46,79);
    cout << "ACMB(46,79) = " << res << endl;

    res = tree->ACMB(tree->getRoot(),25,28);
    cout << "ACMB(25,28) = " << res << endl;

    return 0;
}
