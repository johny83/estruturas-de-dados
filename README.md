# Estruturas de Dados #

Este repositório contêm arquivos utilizados na disciplina de Estruturas de Dados e Algoritmos (DIM0806) do mestrado da Universidade Federal do Rio Grande do Norte (UFRN).

### Projetos ###

* [Algoritmos de Ordenação (MergeSort e QuickSort)](https://bitbucket.org/johny83/estruturas-de-dados/src/master/projeto)

### Listas de Exercícios ###

* Primeira Lista (Árvores Binárias)
* [Segunda Lista  (Árvores Binárias)](https://bitbucket.org/johny83/estruturas-de-dados/src/master/listas/2)