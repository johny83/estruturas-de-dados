/** 
 * \file mergesort.cpp
 *  
 * \brief Implementation of a class that contains a virtual method for
 * for  sorting an  array  of  integer numbers  using  the merge  sort
 * algorithm.
 *
 * \author
 * Marcelo Ferreira Siqueira \n
 * Universidade Federal do Rio Grande do Norte, \n
 * Departamento de Informatica e Matematica Aplicada, \n
 * marcelo at dimap (dot) ufrn (dot) br
 *
 * \version 1.0
 * \date April 2013
 *
 * \attention This program is distributed WITHOUT ANY WARRANTY, and it
 *            may be freely redistributed under the condition that the
 *            copyright notices  are not removed,  and no compensation
 *            is received. Private, research, and institutional use is
 *            free. Distribution of this  code as part of a commercial
 *            system  is permissible ONLY  BY DIRECT  ARRANGEMENT WITH
 *            THE AUTHOR.
 */

#include "mergesort.hpp"         // MergeSort
#include <iostream>              // cout, cerr, endl

/**
 * \fn void MergeSort::sort( int* a , int size )
 *
 * \brief Sort an array of integers using the merge sort algorithm.
 *
 * \param a A pointer to an array of integers.
 * \param size The number of elements in the array.
 */
void
MergeSort::sort(int* a, int size)
{
  // -----------------------------------------------------------------
  //
  // ESCREVA O CÓDIGO DO MERGESORT AQUI!
  //
  // Este  método  deve  ser  uma  espécie  de  merge  sort  bottom-up
  // modificado   para  considerar   subarranjos   contendo  elementos
  // ordenados apenas. Esta  modificação pode descaracterizar o método
  // visto em  sala de aula  completamente. Logo, você deve  pensar em
  // termos de algoritmo primeiro.
  //
  // -----------------------------------------------------------------

  int* b = new int[size];
  int e = 0;
  int m = 0;
  int d = 0;

  int saida = 1;
  int limite = size-1;
  int largura = 0 ;

  do {
    
    if (e == limite) e = m = d = saida = 0;
    
    while ((d<limite) && (largura!=2)) {
      d++;
      if (a[d-1]<=a[d]) {
        if (largura == 0) m++;
        continue;
      }
      largura++;
    }

    if (largura == 2) merge(a, b, e, m, d-1);
    else if ((largura == 1) && (e == 0)) merge(a, b, e, m, d);

    e = m = d;
    saida += largura;

    largura = 0;

  } while (saida!=0);

  return ;

}

/**
 * \fn void MergeSort::merge( int* a , int* b , int e , int m , int d )
 *
 * \brief  Merge two sorted,  consecutive subarrays  of a  given array
 * using an auxiliary array.  The result is a sorted subarray with the
 * same elements of the two  merged subarrays, and whose first element
 * occupies in  the first position of  the left subarray  and the last
 * element occupies the last position of the right subarray.
 *
 * \param a A pointer to an array of integers.
 * \param b A pointer to the auxiliary array.
 * \param e The index of the first element of the left subarray of a.
 * \param m The index of the last element of the left subarray of a.
 * \param d The index of the last element of the right subarray of a.
 */
void
MergeSort::merge(int* a, int* b, int e, int m, int d)
{
  // ESCREVA O CÓDIGO DO PROCEDIMENTO MERGE AQUI!
  int i, j;

  for (i=e; i<=d; i++)
    b[i] = a[i];

  for(i=m+1; i>e; i--) b[i-1] = a[i-1];
  for(j=m; j<d; j++) b[d+m-j] = a[j+1];
  for(int k=e; k <= d; k++)
    if(b[j] < b[i])
      a[k] = b[j--];
    else
      a[k] = b[i++];

  return ;
}
